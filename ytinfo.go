package ytinfo

import (
	"encoding/json"
	"time"

	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/goo-tools/Error"
	"gitlab.com/barry_nevio/goo-tools/HTTPGet"
)

// ItemThumbnail ...
type ItemThumbnail struct {
	URL    string `json:"url"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}

// ChannelInfo ...
type ChannelInfo struct {
	Kind     string `json:"kind"`
	Etag     string `json:"etag"`
	PageInfo struct {
		TotalResults   int `json:"totalResults"`
		ResultsPerPage int `json:"resultsPerPage"`
	} `json:"pageInfo"`
	Items []struct {
		Kind    string `json:"kind"`
		Etag    string `json:"etag"`
		ID      string `json:"id"`
		Snippet struct {
			Title       string    `json:"title"`
			Description string    `json:"description"`
			CustomURL   string    `json:"customUrl"`
			PublishedAt time.Time `json:"publishedAt"`
			Thumbnails  struct {
				Default  ItemThumbnail `json:"default"`
				Medium   ItemThumbnail `json:"medium"`
				High     ItemThumbnail `json:"high"`
				Standard ItemThumbnail `json:"standard"`
				Maxres   ItemThumbnail `json:"maxres"`
			} `json:"thumbnails"`
			Localized struct {
				Title       string `json:"title"`
				Description string `json:"description"`
			} `json:"localized"`
			Country string `json:"country"`
		} `json:"snippet"`
		ContentDetails struct {
			RelatedPlaylists map[string]interface{} `json:"relatedPlaylists"`
		} `json:"contentDetails"`
	} `json:"items"`
}

// GetID ...
func (chi *ChannelInfo) GetID() (string, error) {

	if len(chi.Items) > 0 {
		if len(chi.Items[0].ID) > 0 {
			return chi.Items[0].ID, nil
		}
	}

	return "", Error.New("NO ID FOUND")
}

// GetTitle ...
func (chi *ChannelInfo) GetTitle() (string, error) {

	if len(chi.Items) > 0 {
		if len(chi.Items[0].Snippet.Title) > 0 {
			return chi.Items[0].Snippet.Title, nil
		}
	}

	return "", Error.New("NO TITLE FOUND")
}

// GetDescription ...
func (chi *ChannelInfo) GetDescription() (string, error) {

	if len(chi.Items) > 0 {
		if len(chi.Items[0].Snippet.Description) > 0 {
			return chi.Items[0].Snippet.Description, nil
		}
	}

	return "", Error.New("NO DESCRIPTION FOUND")
}

// GetHighestResThumbnail ...
func (chi *ChannelInfo) GetHighestResThumbnail() (ItemThumbnail, error) {

	if len(chi.Items) > 0 {
		if len(chi.Items[0].Snippet.Thumbnails.Maxres.URL) > 0 {
			return chi.Items[0].Snippet.Thumbnails.Maxres, nil
		}
		if len(chi.Items[0].Snippet.Thumbnails.High.URL) > 0 {
			return chi.Items[0].Snippet.Thumbnails.High, nil
		}
		if len(chi.Items[0].Snippet.Thumbnails.Standard.URL) > 0 {
			return chi.Items[0].Snippet.Thumbnails.Standard, nil
		}
		if len(chi.Items[0].Snippet.Thumbnails.Medium.URL) > 0 {
			return chi.Items[0].Snippet.Thumbnails.Medium, nil
		}
		if len(chi.Items[0].Snippet.Thumbnails.Default.URL) > 0 {
			return chi.Items[0].Snippet.Thumbnails.Default, nil
		}
	}

	return ItemThumbnail{}, Error.New("NO THUMBNAIL FOUND")
}

// GetPlaylistIDByName ...
func (chi *ChannelInfo) GetPlaylistIDByName(playlistName string) (string, error) {
	for _, item := range chi.Items {
		for key, val := range item.ContentDetails.RelatedPlaylists {
			if key == playlistName {
				sVal := cast.ToString(val)
				if len(sVal) > 0 {
					return sVal, nil
				}
			}
		}
	}
	return "", Error.New("PLAYLIST NOT FOUND")
}

// PlaylistItem ...
type PlaylistItem struct {
	Kind    string `json:"kind"`
	Etag    string `json:"etag"`
	ID      string `json:"id"`
	Snippet struct {
		PublishedAt time.Time `json:"publishedAt"`
		ChannelID   string    `json:"channelId"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		Thumbnails  struct {
			Default  ItemThumbnail `json:"default"`
			Medium   ItemThumbnail `json:"medium"`
			High     ItemThumbnail `json:"high"`
			Standard ItemThumbnail `json:"standard"`
			Maxres   ItemThumbnail `json:"maxres"`
		} `json:"thumbnails"`
		ChannelTitle string `json:"channelTitle"`
		PlaylistID   string `json:"playlistId"`
		Position     int    `json:"position"`
		ResourceID   struct {
			Kind    string `json:"kind"`
			VideoID string `json:"videoId"`
		} `json:"resourceId"`
	} `json:"snippet"`
}

// GetHighestResThumbnail ...
func (pli *PlaylistItem) GetHighestResThumbnail() (ItemThumbnail, error) {
	if len(pli.Snippet.Thumbnails.Maxres.URL) > 0 {
		return pli.Snippet.Thumbnails.Maxres, nil
	}
	if len(pli.Snippet.Thumbnails.High.URL) > 0 {
		return pli.Snippet.Thumbnails.High, nil
	}
	if len(pli.Snippet.Thumbnails.Standard.URL) > 0 {
		return pli.Snippet.Thumbnails.Standard, nil
	}
	if len(pli.Snippet.Thumbnails.Medium.URL) > 0 {
		return pli.Snippet.Thumbnails.Medium, nil
	}
	if len(pli.Snippet.Thumbnails.Default.URL) > 0 {
		return pli.Snippet.Thumbnails.Default, nil
	}

	return ItemThumbnail{}, Error.New("NO THUMBNAIL FOUND")
}

// PlaylistInfo ...
type PlaylistInfo struct {
	Kind          string `json:"kind"`
	Etag          string `json:"etag"`
	NextPageToken string `json:"nextPageToken"`
	PageInfo      struct {
		TotalResults   int `json:"totalResults"`
		ResultsPerPage int `json:"resultsPerPage"`
	} `json:"pageInfo"`
	Items []PlaylistItem `json:"items"`
}

// TODO: Add some kind of reusable way to do this...
// GetChannelByID ...
func GetChannelByID(id string, apiKey string) (ChannelInfo, error) {

	var err error
	var channelInfo ChannelInfo

	// Grab the channel info
	channelInfoBodyBytes, err := HTTPGet.AsBytes("https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails&id=" + id + "&key=" + apiKey)
	if err != nil {
		return channelInfo, err
	}

	// Unmarshal the data
	_ = json.Unmarshal(channelInfoBodyBytes, &channelInfo)

	return channelInfo, nil

}

// TODO: Add some kind of reusable way to do this...
// GetChannelByUsername ...
func GetChannelByUsername(username string, apiKey string) (ChannelInfo, error) {

	var err error
	var channelInfo ChannelInfo

	// Grab the channel info
	channelInfoBodyBytes, err := HTTPGet.AsBytes("https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails&forUsername=" + username + "&key=" + apiKey)
	if err != nil {
		return channelInfo, err
	}

	// Unmarshal the data
	_ = json.Unmarshal(channelInfoBodyBytes, &channelInfo)

	return channelInfo, nil

}

// GetPlaylistPagesByID ...
func GetPlaylistPagesByID(playlistID string, apiKey string) ([]PlaylistInfo, error) {

	var err error
	var playlistPages []PlaylistInfo

	// Grab the first playlist page
	playlistInfoPageOneBodyBytes, err := HTTPGet.AsBytes("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=50&playlistId=" + playlistID + "&key=" + apiKey)
	if err != nil {
		return playlistPages, err
	}
	var playlistInfoPageOne PlaylistInfo
	_ = json.Unmarshal(playlistInfoPageOneBodyBytes, &playlistInfoPageOne)
	playlistPages = append(playlistPages, playlistInfoPageOne)

	nextPageToken := playlistPages[0].NextPageToken

	// Loop until there is no next page
	for len(nextPageToken) > 0 {
		playlistInfoBodyBytes, err := HTTPGet.AsBytes("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=50&playlistId=" + playlistID + "&key=" + apiKey + "&pageToken=" + nextPageToken)
		if err != nil {
			return playlistPages, err
		}
		var playlistInfo PlaylistInfo
		_ = json.Unmarshal(playlistInfoBodyBytes, &playlistInfo)

		// SET PAGE TOKEN!
		nextPageToken = playlistInfo.NextPageToken

		playlistPages = append(playlistPages, playlistInfo)
	}

	return playlistPages, nil

}
